let myCone;
let coneBros = [];
// some comment
function setup() {
  createCanvas(1000, 800, WEBGL);
  createEasyCam({distance: 1600});
  normalMaterial();
  for (let i = 0; i < 200; i++) {
    coneBros[i] = new ConeGuy(random(150), random(150), random(150));
  }
}

function draw() {
  background(250);
  // orbitControl();
  for (let i = 0; i < coneBros.length; i++) {
    coneBros[i].run(coneBros);
  }
}
