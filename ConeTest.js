class ConteTest {
  constructor(x, y, z) {
    this.acceleration = createVector(0, 0, 0);
    this.velocity = p5.Vector.random3D();
    this.position = createVector(x, y, z);
    this.r = 3.0;
    this.maxspeed = 3; // Maximum speed
    this.maxforce = 0.05; // Maximum steering force
  }
  run() {
    this.update();
    this.render();
  }

  update() {
    this.position.add(this.velocity);
  }

  render() {
    push();
    translate(this.position);
    cone(3, 9);
    pop();
  }
}
